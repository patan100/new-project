import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import SigninScreen from '../screens/sign-in/Sign-in';
import {createStackNavigator} from '@react-navigation/stack';
import 'react-native-gesture-handler';
import SignUpScreen from '../screens/sign-up/Sign-up';
import ResetPasswordScreen from '../screens/reset-password/Reset-password';
import LoggedInScreen from '../screens/logged-in-screen/Logged-In-Screen';
import {StatusBar, Text, View, Alert} from 'react-native';
import DrawerScreen from './DrawerScreen';
import LocalizedStrings from 'react-native-localization';
import {Notifications} from 'react-native-notifications';
import messaging from '@react-native-firebase/messaging';

const Stack = createStackNavigator();

let strings = new LocalizedStrings({
  'en-US': {
    SignIn: 'Sign In',
    Project: '999',
    ResetPassword: 'Reset Password',
    SignUp: ' Sign Up',
    Home: 'Home',
  },
  ro: {
    Project: '999',
    SignIn: 'Logare',
    SignUp: 'Registrare',
    ResetPassword: 'Ai uitat parola?',
    Home: 'Acasa',
  },
});
function Header() {
  return (
    <View
      style={{
        backgroundColor: '#add9e6',
        width: '100%',
        height: '100%',
        alignSelf: 'center',
      }}>
      <Text>Sign In</Text>
    </View>
  );
}

export default class AppNavigation extends Component {
  constructor(props) {
    super(props);
    // Notifications.events().registerNotificationReceivedForeground(
    //   (notification: Notification, completion) => {
    //     console.log(notification, 'notification');
    //     console.log(
    //       `Notification received in foreground: ${notification.title} : ${
    //         notification.body
    //       }`,
    //     );
    //     completion({alert: true, sound: true, badge: true});
    //   },
    // );
    //
    // Notifications.events().registerNotificationOpened(
    //   (notification: Notification, completion) => {
    //     console.log(notification, 'notification');
    //     console.log(`Notification opened: ${notification.payload}`);
    //     completion();
    //   },
    // );

    // Notifications.postLocalNotification(
    //   {
    //     body: 'Bratka so pornit applicatia',
    //     title: 'Bratan Krassawa',
    //     sound: 'chime.aiff',
    //     category: 'SOME_CATEGORY',
    //     link: 'localNotificationLink',
    //   },
    //   1,
    // );
  }

  componentDidMount() {
    this.getInitial();
  }

  async getInitial() {
    await messaging().registerDeviceForRemoteMessages();
    messaging().onMessage(e => {
      Notifications.postLocalNotification(
        {
          body: 'de la back-end',
          title: 'e la back-end',
          sound: 'chime.aiff',
          category: 'SOME_CATEGORY',
          link: 'localNotificationLink',
        },
        1,
      );
    });
    const token = await messaging().getToken();
    console.log(token, 'tokenFCM');
  }
  changeLanguage = lang => {
    strings.setLanguage(lang);
    this.setState({});
  };

  render() {
    return (
      <NavigationContainer>
        <StatusBar hidden />
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            options={{
              headerStyle: {backgroundColor: '#add9e6', borderWidth: 0},
              headerTitleStyle: {
                textAlign: 'center',
                alignSelf: 'center',
                fontSize: 22,
              },
            }}>
            {props => (
              <SigninScreen
                {...props}
                changeLanguage={this.changeLanguage}
                strings={strings}
              />
            )}
          </Stack.Screen>
          <Stack.Screen
            name="SignUpScreen"
            component={SignUpScreen}
            options={{title: 'Sign Up'}}
          />
          <Stack.Screen
            name="ResetPasswordScreen"
            component={ResetPasswordScreen}
            options={{title: 'Reset Password'}}
          />
          <Stack.Screen
            name="DrawerScreen"
            component={DrawerScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
