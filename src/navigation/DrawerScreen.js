import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import {View} from 'react-native';
import LoggedInScreen from '../screens/logged-in-screen/Logged-In-Screen';
import PackageRequests from '../screens/package-requests/PackageRequests';
import History from '../screens/history/History';

const Drawer = createDrawerNavigator();
function toggleDrawer() {}
function DrawerScreen() {
  return (
    <Drawer.Navigator initialRouteName="LoggedInScreen">
      <Drawer.Screen name="LoggedInScreen" options={{title: 'Home'}}>
        {props => <LoggedInScreen {...props} toggleDrawer={toggleDrawer} />}
      </Drawer.Screen>
      <Drawer.Screen
        name="PackageRequestsScreen"
        component={PackageRequests}
        options={{title: 'Package Requests'}}
      />
      <Drawer.Screen
        name="HistoryScreen"
        component={History}
        options={{title: 'History'}}
      />
    </Drawer.Navigator>
  );
}

export default DrawerScreen;
