import React, {Component} from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
    Modal,
    Alert,
    TouchableHighlight,
    Image,
    Linking,
    Platform
} from "react-native";
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Polyline from '@mapbox/polyline';
import {AuthService} from "../../services/authService";
import {branch} from "baobab-react/higher-order";
import SocketApi from "../../services/socket";
import BackendApi from "../../services/api"
import RouteScreen from "../route/Route";

@branch({
    token: ['token'],
    userId: ['userId'],
    packages: ['packages'],
    startedRoute: ['startedRoute']
})
export default class HomeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            marks: [],
            coords: [],
            cordLatitude: "-6.23",
            cordLongitude: "106.75",
            modalVisible: false,
            modalTitle: 'TEST',
            modalDetails: 'TEST DETAILS',
            ownerId: 0,
            startedRoute: this.props.startedRoute || false
        }
        this.location = {};
    }

    // async getDirections(startLoc, destinationLoc) {
    //     console.log('getDirections');
    //     try {
    //
    //         let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }&key=AIzaSyA2zGAPRdH2mv8Mu392gjfnTqTGRwWkw2U`)
    //         console.log(resp, 'response');
    //         let respJson = await resp.json();
    //         console.log(respJson);
    //         let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
    //         let coords = points.map((point, index) => {
    //             return {
    //                 latitude: point[0],
    //                 longitude: point[1]
    //             }
    //         })
    //         this.setState({coords: coords})
    //         this.setState({x: "true"})
    //         return coords
    //     } catch (error) {
    //         console.log('masuk fungsi', error)
    //         this.setState({x: "error"})
    //         return error
    //     }
    // }
    //
    // mergeLot = () => {
    //     console.log('mergeLot2')
    //     if(this.state.location) {
    //         let concatLot = this.state.location.latitude + "," + this.state.location.longitude;
    //         this.setState({
    //             concat: concatLot
    //         }, () => {
    //             console.log('mergeLot')
    //             this.getDirections(concatLot, "-6.270565,106.759550");
    //         });
    //     }
    // }
    setStartedRoute = () => {
        this.setState({startedRoute: false})
};
    setModalVisible = (modal, title, details, packageId, ownerId) => {
        this.setState({
            modalVisible: modal,
            modalTitle: title,
            modalDetails: details,
            packageId,
            ownerId,
        })
    }
    componentDidUpdate(prevProps) {
        if (JSON.stringify(prevProps.packages) !== JSON.stringify(this.props.packages) && this.props.packages.length !== 0) {
            this.setState({marks: this.props.packages})
        }
    }


    componentDidMount() {
        BackendApi.getStartedRoute().then(res => {
            console.log(res.packageRoutes.length, 'resssss')
            res.packageRoutes.length && this.setState({startedRoute: true, packageRoutes: res.packageRoutes});
            this.props.startedRoute && res.packageRoutes.length && this.setState({startedRoute: true,});
        });

        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.props.token && BackendApi.getAllPackages().then(data => {
                this.setState({
                    marks: data
                })
            });
        });

        Geolocation.getCurrentPosition(info => {
            let location = {
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
            };
            location['latitude'] = info.coords.latitude;
            location['longitude'] = info.coords.longitude;
           this.location = location;
           this.setState({location: location})
            // this.mergeLot();
        }, err => {
            console.log(err, 'err')
        });
    }

    componentWillUnmount(): void {
        this.focusListener();
    }

    getMapRegion = (mark) => ({
        latitude: mark.initialLatitude,
        longitude: mark.initialLongitude,
        latitudeDelta: this.state.location.latitudeDelta,
        longitudeDelta: this.state.location.longitudeDelta,
    });

    callUser = (phone) => {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            // phoneNumber = `telprompt:${phone}`;
            phoneNumber = `tel:${phone}`;
        } else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    };


    render() {
        return (
            <View>
                {this.state.startedRoute ? <RouteScreen packageRoutes={this.state.packageRoutes} setStartedRoute={this.setStartedRoute}/> :
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Weight:{this.state.modalTitle} kg</Text>
                            <Text style={styles.modalText}>{this.state.modalDetails}</Text>
                            <View style={styles.buttonsView}>
                                <TouchableHighlight
                                    style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                    onPress={() => {
                                        this.callUser('076746767')
                                    }}
                                >
                                    <Image style={{alignSelf: 'center', width: 20, height: 20}}
                                           source={{uri: 'https://pngimage.net/wp-content/uploads/2018/05/call-icon-white-png-5.png'}}/>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                    onPress={() => {
                                        BackendApi.createChat(this.state.ownerId).then(res => {
                                            SocketApi.SocketJoinChat(res.data.chatId);
                                            this.setModalVisible(false)
                                            this.props.navigation.navigate('ChatScreen', {chatId: res.data.chatId})
                                        });
                                    }}
                                >
                                    <Image style={{alignSelf: 'center', width: 20, height: 20}}
                                           source={{uri: 'https://flaticons.net/icon.php?slug_category=mobile-application&slug_icon=chat'}}/>
                                </TouchableHighlight>
                                {this.props.userId !== this.state.ownerId ? <TouchableHighlight
                                    style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                    onPress={() => {
                                        this.setModalVisible(false);
                                        BackendApi.requestPackage(this.state.packageId).then(data => {
                                            Alert.alert('Success', data.message)
                                        })
                                        // this.props.navigation.navigate('ExistingPackageScreen', {packageId: this.state.packageId})
                                    }}
                                >
                                    <Text style={styles.textStyle}>Request</Text>
                                </TouchableHighlight> :
                                    <TouchableHighlight
                                        style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                        onPress={() => {
                                            this.setModalVisible(false);
                                            this.props.navigation.navigate('PackageScreen', {packageEdit: true, packageId: this.state.packageId});
                                        }}
                                    >
                                        <Text style={styles.textStyle}>Edit</Text>
                                    </TouchableHighlight>}
                                <TouchableHighlight
                                    style={{...styles.openButton, backgroundColor: "#2196F3"}}
                                    onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}
                                >
                                    <Text style={styles.textStyle}>Close</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </Modal>
                {this.state.location && this.props.token &&
                <View style={styles.container}>
                    <MapView
                        onRegionChangeComplete={(region) => {
                            let location = {...this.location};
                            location['latitudeDelta'] = region.latitudeDelta;
                            location['longitudeDelta'] = region.longitudeDelta;
                            this.location = location;
                        }}
                        loadingEnabled={true}
                        userLocationUpdateInterval={3000}
                        showsUserLocation={true}
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        region={this.state.location}
                        onPress={e => console.log(e.nativeEvent)}
                    >
                        {this.state.marks.map((mark, index) => {
                            return (
                                <Marker
                                    key={index}
                                    onPress={(e) => {
                                        let location = {...this.state.location};
                                        location['latitude'] = e.nativeEvent.coordinate.latitude;
                                        location['longitude'] = e.nativeEvent.coordinate.longitude;
                                        this.location = location;
                                        this.setState({location: location});
                                        BackendApi.getPackageByOwnerId(mark.ownerId).then(data => {
                                            this.setModalVisible(true, mark.weight, mark.description, mark.uuid, mark.ownerId, data.uuid);
                                        })
                                    }}
                                    coordinate={this.getMapRegion(mark)}
                                    title={'Test'}
                                    description={mark.description}
                                    pinColor={this.props.userId === mark.ownerId ?  '#0366d6': '#f64f4f'}
                                />
                            )
                        })}
                        {/*<MapView.Polyline*/}
                        {/*coordinates={this.state.coords}*/}
                        {/*strokeWidth={2}*/}
                        {/*strokeColor="red"/>*/}
                    </MapView>
                </View>
                }
            </View>
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    buttonsView: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 10
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 1000,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        width: 80,
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        marginRight: 5
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
    },
    reviewText: {
        fontSize: 15,
        marginLeft: 5,
        color: '#0645AD',
        marginBottom: 10
    },
    headerRow: {
        display: 'flex',
        flexDirection: 'row',
    }
});