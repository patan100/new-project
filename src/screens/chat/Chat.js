import {View} from "react-native";
import React, {Component} from 'react';
import {Bubble, GiftedChat} from 'react-native-gifted-chat'
import SocketIOClient from 'socket.io-client'
import SocketApi from "../../services/socket";
import BackendApi from "../../services/api";
import {branch} from "baobab-react/higher-order";


@branch({
    token: ['token'],
    userId: ['userId'],
    messageReceived: ['messageReceived']
})


export default class ChatScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
        }
    }

    componentDidMount() {
        BackendApi.getMessages(this.props.route.params.chatId).then(res => {
            let messages = [];
            res.data.messages.forEach(message => {
                let user = {
                    ...message.user,
                    _id: message.user.id,
                    name: message.username,
                    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU'
                }
                let messageToAdd = {
                    ...message,
                    _id: message.uuid
                }
                // messageToAdd['_id'] = message.uuid;
                // messageToAdd['text'] = message.text;
                // messageToAdd['createdAt'] = message.createdAt;
                // user['id'] = message.fromUserId;
                // user['name'] = 'Pedic';
                // user['avatar'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU';
                messageToAdd['user'] = user;
                messages.push(messageToAdd);
            });
            this.setState({messages:messages});
        });
    }

    componentWillUpdate(nextProps) {
        if (nextProps.messageReceived !== this.props.messageReceived) {
            let messages = this.state.messages;
            messages.push(
                {
                    _id: nextProps.messageReceived.uuid,
                    text: nextProps.messageReceived.text,
                    createdAt: nextProps.messageReceived.createdAt,
                    user: {
                        _id: nextProps.messageReceived.fromUserId,
                        name: 'Pedic',
                        avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
                    },
                },
            )
        }

    }

    renderBubble(props) {
        return (
            <Bubble
                {...props}
                textStyle={{
                    left: {
                        color: 'white',
                    }
                }}

                wrapperStyle={{
                    right: {
                        backgroundColor: 'blue'
                    },
                    left: {
                        backgroundColor: 'blue'
                    }
                }}
            />
        )
    }

    onSend(messages = []) {
        this.props.route.params.chatId && SocketApi.SocketSendMessage(this.props.route.params.chatId, messages[0].text, this.props.userId);
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    render() {
        return (
            <GiftedChat
                renderBubble={this.renderBubble}
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={{
                    _id: this.props.userId,
                }}
            />
        )
    }
}
