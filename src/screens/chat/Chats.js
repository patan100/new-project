import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import {Badge, Button} from "native-base";
import moment from 'moment';
import {branch} from "baobab-react/higher-order";
import BackendApi from "../../services/api";

@branch({
    userId: ['userId']
})
export default class ChatsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chats: props.route.params.chats
        };
    }
    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('focus', () => {
            BackendApi.getChats().then(res => {
                this.setState({
                    chats: res
                })
            })
        })
    }

    componentWillUnmount() {
        this.focusListener();
    }


    render() {
        return (
            <View style={styles.container}>
                {this.state.chats.map((chat, index) => {
                    return (
                        <TouchableOpacity key={index}
                                          onPress={() => this.props.navigation.navigate('ChatScreen', {chatId: chat.chatId})}>
                            <View style={styles.reviewsContainer}>
                                <View>
                                    <Image style={{
                                        width: 50, height: 50,
                                        borderRadius: 75,
                                        borderWidth: 3,
                                        marginRight: 5,
                                        marginBottom: 20,
                                    }}
                                           source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU"}}>
                                    </Image>
                                </View>
                                <View style={{flexShrink: 1}}>
                                    <View style={styles.flexRow}>
                                        <Text style={styles.bold}>{chat.username}</Text>
                                        <Text
                                            style={styles.blueText}> {moment(chat.createdAt).format("DD-MM-YYYY HH:mm")}</Text>
                                    </View>
                                    <View style={styles.flexRow}>
                                        <Text
                                            style={chat.seen === 0 ? styles.newText : styles.grayText}>{this.props.userId === chat.fromUserId ? 'You: ' + chat.text : chat.lastMessageUsername + ': ' + chat.text}</Text>
                                        {chat.seen === 0 &&
                                        <Badge danger style={styles.dot}>
                                            <Text style={{color: 'white'}}>New</Text>
                                        </Badge>
                                        }
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
    },
    reviewsContainer: {
        display: 'flex',
        flexDirection: 'row',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 25,
        borderWidth: 1,
        borderColor: 'gainsboro',
        borderRadius: 5,
        backgroundColor: 'white',
    },
    info: {
        paddingRight: 15,
        paddingLeft: 5,
        flexShrink: 1
    },
    bold: {
        alignSelf: 'center',
        fontSize: 17,
        fontWeight: 'bold'
    },
    blueText: {
        alignSelf: 'center',
        color: '#529ff3'
    },
    grayText: {
        color: 'darkgray'
    },
    starStyle: {
        height: 10,
        width: 10
    },
    starContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
    flexRow: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    newText: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    dot: {
        alignSelf: 'flex-start',
        height: 20
    }
});
