import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';
import {Button} from 'native-base';
import BackendApi from '../../services/api';
import SocketApi from "../../services/socket";
import {branch} from "baobab-react/higher-order";

@branch({
  token: ['token'],
  courierRequests: ['courierRequests'],
  ownerRequests: ['ownerRequests']
})

export default class PackageRequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requests: [],
      courierRequests: [],
    };
  }
  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
    if (JSON.stringify(this.state.requests) !== JSON.stringify(this.props.ownerRequests)) {
      this.setState({requests: this.props.ownerRequests})
    }
    if (JSON.stringify(this.state.courierRequests) !== JSON.stringify(this.props.courierRequests)) {
      this.setState({courierRequests: this.props.courierRequests})
    }
  }

  componentDidMount() {
    BackendApi.getPackageRequestsOwner().then(data => {
      this.setState({
        requests: data.packageRequests,
      });
    });
    BackendApi.getPackageRequestsCourier().then(data => {
      this.setState({
        courierRequests: data.packageRequests,
      });
    });
    SocketApi.SocketEmitPackageRequestList();
  }
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.titleRow}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image
                style={styles.backButtonImage}
                source={{
                  uri:
                    'https://cdn.pixabay.com/photo/2016/09/05/10/50/app-1646213_960_720.png',
                }}
              />
            </TouchableOpacity>
            <Text style={styles.header}>Package Requests</Text>
          </View>
          <Text style={styles.header}>Received Package Reqeusts</Text>
          {this.state.requests.map((request, index) => {
            const {courier} = request;
            return (
              <View key={index} style={styles.requestContainer}>
                <View>
                  <Image
                    style={styles.image}
                    source={{
                      uri:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRsdhImGHaHWu-7NCmKofM63gaSukHgtVIJrg&usqp=CAU',
                    }}
                  />
                </View>
                <View style={styles.textContainer}>
                  <Text style={styles.name}>
                    {courier && courier.firstname && courier.lastname
                      ? courier.firstname + ' ' + courier.lastname
                      : courier && courier.email}
                  </Text>
                  <Text style={styles.packageName}>
                    Requested to take the package.
                  </Text>
                  <Text style={styles.packageName}>
                    {request.package.latitude + ' ' + request.package.longitude}
                  </Text>
                  <View style={styles.buttonContainer}>
                    <Button
                      style={styles.button}
                      onPress={() => {
                        BackendApi.setPackageCourier(
                          request.packageId,
                          request.courierId,
                        )
                      }}
                      rounded
                      warning>
                      <Text style={{color: 'white'}}>Accept</Text>
                    </Button>
                    <Button
                      style={styles.button}
                      onPress={() => {
                        let body = {declined: true, courierId: request.courierId};
                        BackendApi.deletePackageRequest(request.packageId, body);
                      }}
                      rounded
                      success>
                      <Text style={{color: 'white'}}>Decline</Text>
                    </Button>
                  </View>
                </View>
              </View>
            );
          })}
          <Text style={styles.header}>Sent Package Requests</Text>
          {this.state.courierRequests.map((request, index) => {
            const {courier} = request;
            return (
              <View key={index} style={styles.requestContainer}>
                <View>
                  <Image
                    style={styles.image}
                    source={{
                      uri:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRsdhImGHaHWu-7NCmKofM63gaSukHgtVIJrg&usqp=CAU',
                    }}
                  />
                </View>
                <View style={styles.textContainer}>
                  <Text style={styles.name}>
                    {courier && courier.firstname && courier.lastname
                      ? courier.firstname + ' ' + courier.lastname
                      : courier && courier.email}
                  </Text>
                  <Text style={styles.packageName}>
                    Requested to take the package.
                  </Text>
                  <Text style={styles.packageName}>
                    {request.package.latitude + ' ' + request.package.longitude}
                  </Text>
                  <View style={styles.buttonContainer}>
                    {request.package.status.name === "Accepted" &&
                    <Button
                      style={styles.button}
                      onPress={() => {
                        // BackendApi.courierConfirm(request.packageId, true);
                        this.props.navigation.navigate('RouteScreen', {
                          courierRequests: this.state.courierRequests,
                          request: request,
                          packageId: request.packageId,
                          initial: {
                            latitude: request.package.initialLatitude,
                            longitude: request.package.initialLongitude,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01,
                          },
                          destination: {
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01,
                            latitude: request.package.destinationLatitude,
                            longitude: request.package.destinationLongitude,
                          }
                        });
                      }}
                      rounded
                      success>
                      <Text style={{color: 'white'}}>Start</Text>
                    </Button>
                    }
                    <Button
                      style={styles.button}
                      onPress={() => {
                        BackendApi.deletePackageRequest(request.packageId);
                      }}
                      rounded
                      success>
                      <Text style={{color: 'white'}}>Delete</Text>
                    </Button>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: 20,
    height: '100%',
  },
  requestContainer: {
    display: 'flex',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#c5c5ca',
    borderRadius: 15,
    padding: 15,
    marginBottom: 20,
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 75,
    borderWidth: 3,
    marginRight: 15,
    marginBottom: 20,
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  button: {
    marginRight: 5,
    marginLeft: 5,
    justifyContent: 'center',
    marginTop: 15,
    width: 100,
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 40,
  },
  backButtonImage: {
    width: 29,
    height: 29,
    borderRadius: 75,
    borderWidth: 3,
    marginRight: 5,
    alignSelf: 'center',
  },
  titleRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  packageName: {
    display: 'flex',
    flexShrink: 1,
    color: '#c5c5ca',
  },
});
