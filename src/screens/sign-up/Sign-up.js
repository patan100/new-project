import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Image,
} from 'react-native';
import {Form, Input, Item} from 'native-base';
import BackendApi from '../../services/api';

let image = require('../../../assets/pexels-photo-561463.jpeg');

export default class SignUpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }

  componentDidMount() {}

  inputValueSet = (text, field) => {
    const user = this.state.user;
    user[field] = text;
    this.setState({
      user,
    });
  };

  validate(fieldName) {
    if (fieldName === 'phone') {
      if (isNaN(parseInt(this.state.user[fieldName]))) {
        return 'Phone should be a number';
      }
    }
    if (fieldName === 'email') {
      if (!this.state.user[fieldName].includes('@')) {
        return 'This is not a valid Email';
      }
    }
    if (this.state.user[fieldName].length <= 0) {
      return 'This field should not be empty.';
    }
  }

  validateField(fieldName) {
    const {user} = this.state;
    let value = this.state.user[fieldName];
    const errors = this.validate(fieldName);
    user[fieldName] = value;

    this.setState({
      [`${fieldName}Error`]: errors,
      user,
    });

    return errors;
  }

  renderTextInput = (field, placeholder) => {
    return (
      <Item
        style={{
          marginTop: 15,
          backgroundColor: '#ffffff28',
          borderRadius: 15,
          width: '80%',
          textAlign: 'center',
          alignSelf: 'center',
        }}>
        <Input
          placeholder={placeholder}
          placeholderTextColor="gray"
          autoCapitalize="none"
          maxLength={30}
          onChangeText={text => {
            this.inputValueSet(text, field);
          }}
        />
      </Item>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: '80%',
            height: '75%',
            backgroundColor: '#ffffff',
            borderRadius: 40,
          }}>
          <Form>
            {this.renderTextInput('firstname', 'First Name')}
            {this.renderTextInput('lastname', 'Last Name')}
            {this.renderTextInput('username', 'Username')}
            {this.renderTextInput('birthDate', 'Birth Date')}
            {this.renderTextInput('email', 'Email')}
            {this.renderTextInput('password', 'Password')}
            {this.renderTextInput('phone', 'Phone')}
          </Form>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
              backgroundColor: 'black',
              borderRadius: 30,
              width: '80%',
              height: '13%',
              justifyContent: 'center',
              marginTop: 15,
            }}
            onPress={() => {
              BackendApi.createNewUser(this.state.user);
              this.props.navigation.goBack();
            }}>
            <Text style={styles.text}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#add9e6',
    alignItems: 'center',
  },
  text: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
