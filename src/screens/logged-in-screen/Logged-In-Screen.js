import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import HomeScreen from '../home/Home';
import {
  Image,
  TouchableOpacity,
  View,
  Text,
  Easing,
  Animated,
} from 'react-native';
import SettingsScreen from '../settings/Settings';
import ChatScreen from '../chat/Chat';
import {Badge} from 'native-base';
import ProfileScreen from '../profile/Profile';
import NotificationsScreen from '../notifications/Notifications';
import ReviewScreen from '../review/Review';
import ChatsScreen from '../chat/Chats';
import {branch} from 'baobab-react/higher-order';
import BackendApi from '../../services/api';
import PackageScreen from '../package/Package';
import PackageRequests from '../package-requests/PackageRequests';
import ExistingPackage from '../package/ExistingPackage';
import UserPackages from '../package/UserPackages';
import RouteScreen from '../route/Route';
import History from '../history/History';

const Stack = createStackNavigator();

export class Header extends Component {
  render() {
    return (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.toggleDrawer();
          }}>
          <Image
            style={{
              width: 30,
              height: 30,
            }}
            source={{uri: 'https://www.indiawoodcrafts.com/img/menuIcon.png'}}
          />
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
            }}
            onPress={() => {
              this.props.navigation.navigate('PackageScreen');
            }}>
            <Image
              style={{
                width: 35,
                height: 35,
                marginRight: 10,
              }}
              source={{
                uri:
                  'https://www.freepngimg.com/download/map/67106-map-tracking-location-system-mark-green-red.png',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
            }}
            onPress={() => {
              BackendApi.getChats().then(res => {
                this.props.navigation.navigate('ChatsScreen', {
                  chats: res.reverse(),
                });
              });
              //navigate to chats
            }}>
            <Image
              style={{
                width: 40,
                height: 40,
                marginRight: 10,
              }}
              source={{
                uri:
                  'https://activejamm.com/wp-content/uploads/2016/07/MESSAGINGICONS-300x300.png',
              }}
            />
            <Badge alert style={{height: 20, position: 'absolute', right: 1}}>
              <Text>1</Text>
            </Badge>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
            }}
            onPress={() => {
              this.props.navigation.navigate('ProfileScreen');
            }}>
            <Image
              style={{
                width: 40,
                height: 40,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
              }}
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
              }}
            />
            <Badge alert style={{height: 20, position: 'absolute', right: 1}}>
              <Text>1</Text>
            </Badge>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

let ChatHeader = () => {
  return (
    <View>
      <Image
        style={{
          width: 40,
          height: 40,
          borderRadius: 75,
          borderWidth: 3,
        }}
        source={{
          uri:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
        }}
      />
    </View>
  );
};

export default class LoggedInScreen extends Component {
  render() {
    return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            headerTitle: props => (
              <Header {...props} navigation={this.props.navigation} />
            ),
          }}
        />
        <Stack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
          options={{title: 'Settings'}}
        />
        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NotificationsScreen"
          component={NotificationsScreen}
          options={{title: 'Notifications'}}
        />
        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
          options={{headerTitle: props => <ChatHeader />}}
        />
        <Stack.Screen name="ReviewScreen" component={ReviewScreen} />
        <Stack.Screen
          name="ChatsScreen"
          component={ChatsScreen}
          options={{title: 'Messages'}}
        />
        <Stack.Screen
          name="PackageScreen"
          component={PackageScreen}
          options={{title: 'Add a package'}}
        />
        <Stack.Screen
          name="PackageRequestsScreen"
          component={PackageRequests}
          options={{title: 'Requests'}}
        />
        <Stack.Screen
          name="HistoryScreen"
          component={History}
          options={{title: 'History'}}
        />
        <Stack.Screen
          name="ExistingPackageScreen"
          component={ExistingPackage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="UserPackagesScreen"
          component={UserPackages}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RouteScreen"
          component={RouteScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  }
}
