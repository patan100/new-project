import React, {Component} from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {Badge} from 'native-base';

export default class NotificationsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
    };
  }

  render() {
    return (
      <View>
        <View>
          <Text style={styles.text}>Mark all as read</Text>
        </View>
        <View>
          <View style={styles.notificationBlock}>
            <Image
              style={{
                width: 55,
                height: 55,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                alignSelf: 'center',
                marginLeft: 20,
              }}
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
              }}
            />
            <View style={styles.textBlock}>
              <Text style={styles.textHeader}> Test left you a message.</Text>
              <Text style={styles.time}> 10:45 AM</Text>
            </View>
            <Badge danger style={styles.dot}>
              <Text style={{color: 'white'}}>New</Text>
            </Badge>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f4f7',
  },
  text: {
    color: '#c6c8cd',
    fontSize: 16,
    alignSelf: 'flex-end',
    marginRight: 15,
    marginTop: 15,
  },
  textHeader: {
    color: 'black',
    fontSize: 16,
    alignSelf: 'flex-end',
    marginRight: 15,
    marginTop: 15,
  },
  time: {
    color: '#c6c8cd',
    fontSize: 16,
  },
  notificationBlock: {
    borderLeftWidth: 8,
    borderLeftColor: '#3375f5',
    width: '90%',
    height: 100,
    backgroundColor: 'white',
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  textBlock: {
    flexDirection: 'column',
  },
  dot: {
    alignSelf: 'flex-start',
    marginTop: 30,
    marginRight: 20,
    height: 20,
  },
});
