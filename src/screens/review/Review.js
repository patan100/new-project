import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  Modal,
} from 'react-native';
import {Badge, Button, Input, Item, Label} from 'native-base';
import BackendApi from '../../services/api';
import SocketApi from '../../services/socket';

export default class ReviewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stars: [1, 2, 3, 4, 5],
      addedReviewStars: 0,
      ratings: [],
      modalVisible: false,
      reviewDescrition: '',
      user: {},
    };
  }

  componentDidMount(): void {
    // BackendApi.addReview(3, 'Crutoi Patan', '3').then(data => {
    //     console.log(data, 'data')
    // });
    // BackendApi.getUser(this.props.route.params.ownerId).then(data => {
    //   console.log(data, 'dta');
    //   this.setState({user: data.data.user});
    // })
    // BackendApi.getReviewsByUserId(this.props.route.params.ownerId).then(
    //   data => {
    //     console.log(data, 'getReviews');
    //     this.setState({ratings: data.reviews});
    //     // BackendApi.editReview(data['reviews'][0].uuid, null, 'Changed Babe!').then(response => {
    //     //     console.log(response, 'TheResponse')
    //     // })
    //     //     BackendApi.deleteReview(data['reviews'][0].uuid).then(data => {
    //     //         console.log(data, 'theResponse')
    //     //     })
    //   },
    // );
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView} />
          </View>
        </Modal>
        <View style={styles.header}>
          <View>
            <Image
              style={{
                width: 115,
                height: 115,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 50,
              }}
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
              }}
            />
          </View>
          <View style={styles.info}>
            <Text style={styles.bold}>{this.state.user.username}</Text>
            <Text style={styles.grayText}>
              Austalian Truck Driver, has been able to work with more than 200
              job givers, and has always been able to finish his duties on time.
            </Text>
            {/*<Button*/}
            {/*  onPress={() => {*/}
            {/*    this.setState({*/}
            {/*      modalVisible: true,*/}
            {/*    });*/}
            {/*  }}*/}
            {/*  rounded*/}
            {/*  info*/}
            {/*  style={{justifyContent: 'center', marginTop: 15}}>*/}
            {/*  <Text style={{color: 'white'}}>Add a Review</Text>*/}
            {/*</Button>*/}
          </View>
        </View>
        {this.state.ratings.map((rating, index) => {
          return (
            <View style={styles.reviewsContainer}>
              <View>
                <Image
                  style={{
                    width: 80,
                    height: 80,
                    borderRadius: 75,
                    borderWidth: 3,
                    marginRight: 5,
                    marginBottom: 50,
                  }}
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
                  }}
                />
              </View>
              <View style={{flexShrink: 1}}>
                <Text style={styles.bold}>
                  {' '}
                  {rating && rating.toUser && rating.toUser.username}
                </Text>
                <Text style={styles.blueText}>
                  {' '}
                  {rating && rating.createdAt}
                </Text>
                <View style={styles.starContainer}>
                  {this.state.stars.map((star, index) => {
                    let source =
                      'https://raw.githubusercontent.com/ihak/star-rating-react-native/master/StarRating/star-filled.png';
                    if (index >= rating.stars) {
                      source =
                        'https://raw.githubusercontent.com/ihak/star-rating-react-native/master/StarRating/star-unfilled.png';
                    }
                    return (
                      <Image style={styles.starStyle} source={{uri: source}} />
                    );
                  })}
                </View>
                <Text style={styles.grayText}>
                  {rating && rating.description}
                </Text>
              </View>
            </View>
          );
        })}
        {/*<View style={styles.reviewsContainer}></View>*/}
        {/*<View style={styles.reviewsContainer}></View>*/}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    paddingRight: 15,
    paddingLeft: 35,
    paddingTop: 25,
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  reviewsContainer: {
    display: 'flex',
    flexDirection: 'row',
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 25,
    borderWidth: 1,
    borderColor: 'gainsboro',
    borderRadius: 5,
    backgroundColor: 'whitesmoke',
  },
  info: {
    paddingRight: 15,
    paddingLeft: 5,
    flexShrink: 1,
  },
  bold: {
    fontWeight: 'bold',
  },
  blueText: {
    color: '#529ff3',
  },
  grayText: {
    color: 'darkgray',
  },
  starStyle: {
    height: 10,
    width: 10,
  },
  modalStarStyle: {
    height: 35,
    width: 35,
  },
  starContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 5,
    alignSelf: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    display: 'flex',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '100%',
    height: '70%',
    justifyContent: 'space-between',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  button: {
    marginRight: 5,
    marginLeft: 5,
    justifyContent: 'center',
    marginTop: 15,
    width: 100,
  },
  input: {
    alignSelf: 'center',
    fontSize: 17,
    width: '100%',
  },
});
