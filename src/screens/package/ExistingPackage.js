import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Platform,
    Image,
    TouchableOpacity,
    TextInput,
    Picker,
    TouchableHighlight
} from "react-native";
import {Dimensions} from "react-native";
import BackendApi from "../../services/api";
import {branch} from "baobab-react/higher-order";

const screenHeight = Math.round(Dimensions.get('window').height);

@branch({
    userId: ['userId']
})
export default class ExistingPackageScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentWillMount() {
        this.props.route.params.packageId && BackendApi.getPackageById(this.props.route.params.packageId).then(data => {
            this.setState({
                package: data
            })
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('Home')
                }
                } style={styles.backButton}>
                    <Image  style={styles.backButtonImage} source={{uri: 'https://www.materialui.co/materialIcons/navigation/arrow_back_white_192x192.png'}}/>
                </TouchableOpacity>
                <View style={styles.imageContainer}>
                    <Image style={styles.image} source={{uri: 'https://www.vermit-group.com/portals/vermit-en/Images/Uporaba/Embalaza/packagingIcon.png'}}/>
                </View>
                <View style={styles.descriptionContainer}>
                    <View style={styles.statusBar}>
                        <Text style={styles.status}>Status: Pending</Text>
                    </View>
                    <Text style={styles.packageName}>{this.state.package && this.state.package.initialLatitude + ' ' + this.state.package.initialLongitude}</Text>
                    <Text style={styles.userName}>Owner: { this.state.package && this.state.package.owner.username}</Text>
                    <View
                        style={{
                            borderBottomColor: '#a7a6a6',
                            alignSelf: 'stretch',
                            borderBottomWidth: StyleSheet.hairlineWidth,
                            marginTop: 20,
                            marginBottom: 40,
                        }}
                    />
                    <Text style={styles.packageName}>Description</Text>
                    <Text style={styles.userName}>{ this.state.package && this.state.package.description}</Text>
                    <Text style={styles.packageName}>Weight</Text>
                    <Text style={styles.userName}>{ this.state.package && this.state.package.weight} kg</Text>
                    { this.state.package && this.props.userId !== this.state.package.ownerId && this.props.navigation && <TouchableHighlight
                        style={{...styles.openButton, backgroundColor: "#f06263"}}
                        onPress={() => {
                            this.state.package && this.state.package.uuid && BackendApi.requestPackage(this.state.package.uuid).then(data => {
                                this.props.navigation.navigate('Home')
                            })
                        }}
                    >
                        <Text style={styles.textStyle}>Request</Text>
                    </TouchableHighlight>}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height: screenHeight + 50,
        width: 400,
        backgroundColor: '#f06263',
    },
    image: {
        height: 170,
        width: 200,
        alignSelf: 'center',
    },
    imageContainer: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
    },
    descriptionContainer: {
        flex: 2,
        borderRadius: 40,
        height: screenHeight / 2,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderColor: 'white',
        backgroundColor: 'white',
        position: 'relative',
        padding: 30
    },
    packageName: {
        fontSize: 18,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        marginTop: 10,
        marginBottom: 10
    },
    statusBar: {
        height: 30,
        borderRadius: 50,
        backgroundColor: '#67e370',
        position: 'absolute',
        top: 0,
        justifyContent: 'center',
        padding: 10
    },
    status: {
        alignSelf: 'center',
        color: 'white'
    },
    userName: {
        color: 'gray',
        alignSelf: 'flex-start',
        fontSize: 16
    },
    backButton: {
        position: 'absolute',
        top: 30,
        left: 20
    },
    backButtonImage: {
        height: 45,
        width: 45
    },
    openButton: {
        backgroundColor: "#F194FF",
        width: 150,
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        marginRight: 5,
        position: 'absolute',
        bottom: 40
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },

});
