import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Platform,
  Image,
  TouchableOpacity,
  TextInput,
  Picker,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {AuthService} from '../../services/authService';
import Geolocation from '@react-native-community/geolocation';
import {Dimensions} from 'react-native';
import {divide} from 'react-native-reanimated';
import BackendApi from '../../services/api';
import SocketApi from '../../services/socket';

const screenHeight = Math.round(Dimensions.get('window').height);

export default class PackageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mark: {},
      initial: true,
      package: {},
    };
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      info => {
        let location = {
          latitudeDelta: 0.01,
          longitudeDelta: 0.01,
        };
        location.latitude = info.coords.latitude;
        location.longitude = info.coords.longitude;
        this.setState({
          location: location,
        });
        // this.mergeLot();
      },
      err => {
        console.log(err, 'err');
      },
    );
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.packageEdit &&
      this.props.route.params.packageId
    ) {
      BackendApi.getPackageById(this.props.route.params.packageId).then(
        data => {
          this.setState({
            package: data,
            weight: data.weight.toString(),
            description: data.description,
            mark: {
              latitude: data.initialLatitude,
              longitude: data.initialLongitude,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            },
          });
        },
      );
    }

    // this.watchID = Geolocation.watchPosition(
    //     position => {
    //         // console.log(position, 'position')
    //         const {coordinate, routeCoordinates, distanceTravelled} = this.state;
    //         const {latitude, longitude} = position.coords;
    //
    //         const location = {
    //             latitude,
    //             longitude,
    //             latitudeDelta: 0.01,
    //             longitudeDelta: 0.01,
    //         };
    //         if (Platform.OS === "android") {
    //             if (this.marker) {
    //                 this.marker._component.animateMarkerToCoordinate(
    //                     location,
    //                     500
    //                 );
    //             }
    //         } else {
    //             coordinate.timing(location).start();
    //         }
    //         this.setState({
    //             location,
    //         });
    //     },
    //     error => console.log(error),
    //     {enableHighAccuracy: true, timeout: 10000, maximumAge: 0, distanceFilter: 1})
  }

  getMapRegion = () => ({
    latitude: this.state.mark.latitude,
    longitude: this.state.mark.longitude,
    latitudeDelta: 0.01,
    longitudeDelta: 0.01,
  });
  setCurrentPosition = () => {
    if (this.state.initial) {
      this.setState({
        locationLatitude: this.state.location.latitude,
        locationLongitude: this.state.location.longitude,
        mark: this.state.location,
      });
    } else {
      this.setState({
        destinationLatitude: this.state.location.latitude,
        destinationLongitude: this.state.location.longitude,
        mark: this.state.location,
      });
    }
  };

  addPackage = () => {
    if (!this.state.initial) {
      this.state.description &&
        this.state.weight &&
        this.state.destinationLongitude &&
        BackendApi.addPackage({
          initialLatitude: this.state.locationLatitude,
          initialLongitude: this.state.locationLongitude,
          description: this.state.description,
          weight: this.state.weight,
          destinationLongitude: this.state.destinationLongitude,
          destinationLatitude: this.state.destinationLatitude,
        }).then(res => {
          this.state.description &&
            this.state.weight &&
            this.props.navigation.navigate('ExistingPackageScreen', {
              packageId: res.data.newPackage.uuid,
            });
        });
    } else {
      this.state.description &&
        this.state.weight &&
        this.state.locationLatitude &&
        this.setState({initial: false, mark: {}});
    }
  };
  editPackage = () => {
    if (!this.state.initial) {
      this.state.description &&
        this.state.weight &&
        this.state.destinationLongitude &&
        BackendApi.editPackage(
          {
            initialLatitude: this.state.locationLatitude,
            initialLongitude: this.state.locationLongitude,
            description: this.state.description,
            weight: this.state.weight,
            destinationLongitude: this.state.destinationLongitude,
            destinationLatitude: this.state.destinationLatitude,
          },
          this.state.package.uuid,
        ).then(res => {
          this.props.navigation.navigate('ExistingPackageScreen', {
            packageId: this.state.package.uuid,
          });
        });
    } else {
      this.state.description &&
        this.state.weight &&
        this.state.locationLatitude &&
        this.setState({
          initial: false,
          mark: {
            latitude: this.state.package.destinationLatitude,
            longitude: this.state.package.destinationLongitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          },
        });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.location && (
          <View style={styles.map}>
            <MapView
              onRegionChangeComplete={region => {
                this.setState({
                  location: region,
                });
              }}
              showsUserLocation={true}
              followUserLocation={true}
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              region={this.state.location}
              onPress={e => {
                let mark = {
                  latitude: e.nativeEvent.coordinate.latitude,
                  longitude: e.nativeEvent.coordinate.longitude,
                  latitudeDelta: 0.01,
                  longitudeDelta: 0.01,
                };
                if (this.state.initial) {
                  this.setState({
                    locationLatitude: e.nativeEvent.coordinate.latitude,
                    locationLongitude: e.nativeEvent.coordinate.longitude,
                    mark: mark,
                  });
                } else {
                  this.setState({
                    destinationLatitude: e.nativeEvent.coordinate.latitude,
                    destinationLongitude: e.nativeEvent.coordinate.longitude,
                    mark: mark,
                  });
                }
              }}>
              {this.state.mark.latitude && (
                <Marker
                  draggable
                  onPress={() => {}}
                  coordinate={this.state.mark.latitude && this.getMapRegion()}
                  title={'Pedic'}
                  description={'De pe ciocana'}
                  onDragEnd={e => {
                    let mark = {
                      latitude: e.nativeEvent.coordinate.latitude,
                      longitude: e.nativeEvent.coordinate.longitude,
                      latitudeDelta: 0.01,
                      longitudeDelta: 0.01,
                    };
                    if (this.state.initial) {
                      this.setState({
                        locationLatitude: e.nativeEvent.coordinate.latitude,
                        locationLongitude: e.nativeEvent.coordinate.longitude,
                        mark: mark,
                      });
                    } else {
                      this.setState({
                        destinationLatitude: e.nativeEvent.coordinate.latitude,
                        destinationLongitude:
                          e.nativeEvent.coordinate.longitude,
                        mark: mark,
                      });
                    }
                  }}
                />
              )}
            </MapView>
            <View style={styles.footer}>
              <Text
                style={[
                  styles.text,
                  !this.state.initial && styles.justifyContentCenter,
                ]}>
                {this.state.initial
                  ? 'Press the map to set the initial location.'
                  : 'Press the map to set the final destination.'}
              </Text>
              {/*<Text style={styles.smallText}>*/}
              {/*  Or choose the country and type the street.*/}
              {/*</Text>*/}
              {this.state.initial && (
                <>
                  <TextInput
                    style={styles.input}
                    keyboardType={'numeric'}
                    underlineColorAndroid="red"
                    placeholder="Weight"
                    placeholderTextColor="grey"
                    autoCapitalize="none"
                    value={this.state.weight}
                    onChangeText={text => {
                      this.setState({
                        weight: text,
                      });
                    }}
                  />
                  <TextInput
                    value={this.state.description}
                    style={styles.input}
                    underlineColorAndroid="red"
                    placeholder="Description"
                    placeholderTextColor="grey"
                    autoCapitalize="none"
                    onChangeText={text => {
                      this.setState({
                        description: text,
                      });
                    }}
                  />
                </>
              )}
              <TouchableOpacity
                style={{position: 'absolute', bottom: 50}}
                onPress={() => {
                  this.props.route &&
                  this.props.route.params &&
                  this.props.route.params.packageEdit
                    ? this.editPackage()
                    : this.addPackage();
                }}>
                <Image
                  style={styles.image}
                  source={{
                    uri:
                      'https://cdn1.iconfinder.com/data/icons/ui-navigation-1/152/confirm-512.png',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        <TouchableOpacity
          onPress={() => this.setCurrentPosition()}
          style={{
            position: 'absolute',
            top: 15,
            left: 10,
            backgroundColor: 'red',
            borderRadius: 50,
            padding: 6,
          }}>
          <Text style={{fontSize: 12, color: 'white'}}>
            Set current location
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: screenHeight,
    width: 400,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
    height: '35%',
    textAlign: 'center',
  },
  justifyContentCenter: {
    marginTop: 60,
  },
  text: {
    fontSize: 15,
    marginTop: 10,
    letterSpacing: 2,
  },
  smallText: {
    fontSize: 16,
    marginTop: 20,
    color: 'gray',
  },
  image: {
    height: 50,
    width: 50,
    marginTop: 5,
  },
  input: {
    margin: 5,
    height: 40,
    width: 200,
  },
});
