import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Animated,
  LayoutAnimation,
  PanResponder,
  Platform,
  UIManager,
} from 'react-native';
import BackendApi from '../../services/api';
import MapView, {PROVIDER_GOOGLE, Callout} from 'react-native-maps';
import {Marker} from 'react-native-maps';
import {Button, Input, Item, ActionSheet} from 'native-base';
import {Dimensions} from 'react-native';
import {StateService} from '../../services/state/stateService';

export default class RouteScreen extends Component {
  constructor(props) {
    super(props);
    this.marker = null;
    this.state = {
      modalVisible: true,
      finished: false,
      reviewDescription: '',
      stars: [1, 2, 3, 4, 5],
      addedReviewStars: 0,
      stops: [],
      options: [],
      filteredRequests: [],
      started: false,
      currentRoute: {},
      translateValue: new Animated.Value(0),
    };

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {
        this.state.translateValue.setValue(Math.max(0, 0 + gestureState.dy)); //step 1
      },
      onPanResponderRelease: (e, gesture) => {
        const shouldOpen = gesture.vy <= 0;
        Animated.timing(this.state.translateValue, {
          toValue: shouldOpen ? 0 : this.state.dimensions.height - 40,
          duration: 200,
          useNativeDriver: true,
        }).start(); //step 2
      },
    });

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    if (
      prevState &&
      JSON.stringify(prevState.stops) !== JSON.stringify(this.state.stops)
    ) {
      this.setState({currentRoute: this.state.stops[0]});
    }
  }

  componentDidMount(): void {
    BackendApi.getPackageRequestsCourier().then(data => {
      this.setState({courierRequests: data.packageRequests}, () =>
        BackendApi.getStartedRoute().then(data => {
          console.log(data, 'data');
          if (data.packageRoutes.length) {
            this.setState({stops: data.packageRoutes, started: true}, () => {
              this.getDropDownOptions(),
                BackendApi.getPackageById(
                  this.props.route &&
                    this.props.params &&
                    this.props.route.params.packageId
                    ? this.props.route.params.packageId
                    : this.state.stops[0].packageId,
                ).then(data => {
                  this.setState({ownerId: data.ownerId});
                });
            });
          } else {
            this.getDropDownOptions();
          }
        }),
      );
    });
  }

  getDropDownOptions = () => {
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.request
    ) {
      this.setState({
        stops: [this.props.route.params.request],
      });
    }

    this.setState({
      currentRoute: this.state.stops[0],
    });

    let options = this.state.courierRequests.filter(request => {
      return request.package.status.name === 'Accepted';
    });
    options = options.filter(option => {
      if (
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.request.packageId
      ) {
        return option.packageId !== this.props.route.params.request.packageId;
      } else {
        return option.packageId !== this.state.stops[0].packageId;
      }
    });
    let filteredOptions = [];
    this.setState({filteredRequests: options});
    options.map((request, index) => {
      filteredOptions[index] = request.package.description;
    });
    filteredOptions.push('Cancel');
    this.setState({options: filteredOptions});
  };

  onRegionChangeComplete = () => {
    if (this.marker && this.marker.current && this.marker.current.showCallout) {
      this.marker.current.showCallout();
    }
  };

  setMarkerRef = ref => {
    this.marker = ref;
  };

  addStop = buttonIndex => {
    let stops = [...this.state.stops];
    let option = {};
    let options = [...this.state.options];

    if (
      this.state.courierRequests &&
      buttonIndex !== this.state.options.length - 1
    ) {
      option = this.state.filteredRequests.filter(request => {
        return request.package.description === this.state.options[buttonIndex];
      });
    }
    if (!JSON.stringify(stops).includes(JSON.stringify(option[0]))) {
      stops.push(option[0]);
    }
    options = options.filter(option => {
      return option !== this.state.options[buttonIndex];
    });
    if (this.state.started) {
      BackendApi.addStopToStartedRoute(stops).then(res => {});
    }
    this.setState({stops: stops, options});
  };

  onLayout = event => {
    let {width, height} = event.nativeEvent.layout;
    this.setState({dimensions: {width, height}});
  };

  finishRoute = () => {
    let requestStops = [...this.state.stops];
    requestStops.map((stop, index) => {
      requestStops[index] = stop.packageId;
    });
    if (this.state.started && this.state.stops.length) {
      BackendApi.setFinishedRoute(this.state.currentRoute.packageId).then(
        () => {
          this.setState({finished: true});
        },
      );
    }
    if (this.state.started && !this.state.stops.length) {
      BackendApi.getPackageRequestsOwner().then(data => {
        StateService.setField('ownerRequests', data.packageRequests);
      });
      BackendApi.getPackageRequestsCourier().then(data => {
        StateService.setField('courierRequests', data.packageRequests);
      });

      this.props.setStartedRoute
        ? this.props.setStartedRoute()
        : this.props.navigation.navigate('Home');
    }

    if (!this.state.started) {
      BackendApi.setStartedRoute(requestStops).then(res => {
        this.setState({
          started: true,
        });
      });
    }
  };

  setPackageDelivered = () => {
    console.log(this.state.currentRoute, 'currentRoute');
    BackendApi.addReview(
      this.state.currentRoute.package.ownerId,
      this.state.reviewDescription,
      this.state.addedReviewStars,
      this.state.currentRoute.packageId,
    ).then(() => {
      let stops = this.state.stops.filter(stop => {
        return stop.packageId !== this.state.currentRoute.packageId;
      });
      BackendApi.getPackageRequestsOwner().then(data => {
        StateService.setField('ownerRequests', data.packageRequests);
      });
      BackendApi.getPackageRequestsCourier().then(data => {
        StateService.setField('courierRequests', data.packageRequests);
      });
      if (this.state.started && stops.length) {
        stops.length && this.setState({currentRoute: stops[0]});
        Animated.timing(this.animatedValue, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true,
        }).start(() => {
          this.setState(
            {
              stops,
              finished: false,
              addedReviewStars: 0,
              reviewDescription: '',
            },
            () => {
              LayoutAnimation.configureNext(
                LayoutAnimation.Presets.easeInEaseOut,
              );
            },
          );
        });
      }
      if (this.state.started && !stops.length) {
        this.props.navigation.navigate('Home');
      }
    });
  };

  showActionSheet = () => {
    ActionSheet.show(
      {
        options: this.state.options,
        cancelButtonIndex: this.state.options.length - 1,
        title: 'Add a package to the route.',
      },
      buttonIndex => {
        buttonIndex !== this.state.options.length - 1 &&
          this.addStop(buttonIndex);
      },
    );
  };

  render() {
    const width = Dimensions.get('window').width;
    return (
      <View style={styles.container}>
        <MapView
          onRegionChangeComplete={this.onRegionChangeComplete}
          loadingEnabled={true}
          userLocationUpdateInterval={3000}
          showsUserLocation={true}
          followUserLocation={true}
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.initial
              ? this.props.route.params.initial
              : {
                  latitude: this.props.packageRoutes[0].package.initialLatitude,
                  longitude: this.props.packageRoutes[0].package
                    .initialLongitude,
                  latitudeDelta: 0.01,
                  longitudeDelta: 0.01,
                }
          }>
          {this.state.stops.map((stop, index) => {
            return (
              <View key={index}>
                <Marker
                  onPress={() => {}}
                  coordinate={{
                    latitude: stop.package.initialLatitude,
                    longitude: stop.package.initialLongitude,
                  }}
                  title={'Pick up'}
                  description={`Street initial ${index}`}>
                  <Image
                    style={{height: 55, width: 55}}
                    source={{
                      uri:
                        'https://www.nicepng.com/png/full/101-1015767_map-marker-circle-png.png',
                    }}
                  />
                  <Callout>
                    <View style={styles.callout} />
                  </Callout>
                </Marker>
                <Marker
                  ref={this.setMarkerRef}
                  coordinate={{
                    latitude: stop.package.destinationLatitude,
                    longitude: stop.package.destinationLongitude,
                  }}
                  title={'Drop off'}
                  description={`Street destination ${index}`}>
                  <Image
                    style={{height: 45, width: 45}}
                    source={{
                      uri: 'https://carto.com/blog/img/authors/carto-logo.png',
                    }}
                  />
                </Marker>
              </View>
            );
          })}
        </MapView>
        <Animated.View
          {...this.panResponder.panHandlers}
          style={[
            {position: 'absolute', bottom: 0, width: '100%'},
            {
              transform: [
                {
                  translateY: this.state.translateValue,
                },
              ],
            },
          ]}>
          <View style={styles.centeredView} onLayout={this.onLayout}>
            <View style={styles.modalView}>
              <TouchableOpacity style={styles.line} />
              {this.state.finished ? (
                <View
                  style={{
                    paddingRight: 35,
                    paddingLeft: 35,
                    paddingBottom: 35,
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontWeight: 'bold',
                      fontSize: 19,
                      marginBottom: 20,
                    }}>
                    Leave a rating
                  </Text>
                  <View style={{flexShrink: 1, textAlign: 'center'}}>
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 17,
                        marginBottom: 20,
                      }}>
                      How would you rate the communication between you and John?
                    </Text>
                  </View>
                  <View style={styles.starContainer}>
                    {this.state.stars.map((star, index) => {
                      let source =
                        'https://raw.githubusercontent.com/ihak/star-rating-react-native/master/StarRating/star-filled.png';
                      if (index >= this.state.addedReviewStars) {
                        source =
                          'https://raw.githubusercontent.com/ihak/star-rating-react-native/master/StarRating/star-unfilled.png';
                      }
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() => {
                            this.setState({
                              addedReviewStars: index + 1,
                            });
                          }}>
                          <Image
                            style={styles.modalStarStyle}
                            source={{uri: source}}
                          />
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View
                    style={{
                      borderColor: 'grey',
                      marginBottom: 20,
                    }}>
                    <Item inlineLabel>
                      <Input
                        style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Type something"
                        placeholderTextColor="grey"
                        multiline={true}
                        onChangeText={text =>
                          this.setState({reviewDescription: text})
                        }
                        value={this.state.reviewDescription}
                      />
                    </Item>
                  </View>
                  <View style={styles.buttonContainer}>
                    <Button
                      style={styles.button}
                      onPress={() => {
                        this.setPackageDelivered();
                      }}
                      rounded
                      info>
                      <Text style={{color: 'white'}}>Finish Delivery</Text>
                    </Button>
                  </View>
                </View>
              ) : (
                <View style={styles.buttonsView}>
                  <View style={styles.textView}>
                    <Text style={[styles.reviewText]}>
                      Starting a new delivery route.
                    </Text>
                    <Text
                      style={[
                        styles.modalText,
                        {fontWeight: 'bold', marginBottom: 10},
                      ]}>
                      Add packages to deliver.
                    </Text>
                    {console.log(this.state.stops, 'stps')}
                    {this.state.stops.map((stop, index) => {
                      return (
                        <View style={[styles.stop]} key={index}>
                          <View style={{flex: 1, justifyContent: 'center'}}>
                            <Image
                              style={{
                                width: 12,
                                height: 12,
                                marginLeft: 20,
                              }}
                              source={{
                                uri:
                                  'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Bullet_%28typography%29.svg/600px-Bullet_%28typography%29.svg.png',
                              }}
                            />
                          </View>
                          <View style={{flex: 2}}>
                            <Text
                              style={{
                                color: 'black',
                                fontSize: 17,
                                fontWeight: 'bold',
                              }}>
                              {stop.package.description}
                            </Text>
                            <Text
                              style={{
                                color: '#bcbcbc',
                                fontSize: 14,
                              }}>
                              12 Km
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                    <View style={{height: 50, alignSelf: 'center'}}>
                      <Button
                        transparent
                        onPress={() => this.showActionSheet()}>
                        <Image
                          style={{width: 30, height: 30}}
                          source={{
                            uri:
                              'https://icon-library.com/images/blue-plus-icon/blue-plus-icon-2.jpg',
                          }}
                        />
                      </Button>
                    </View>
                  </View>
                  <TouchableHighlight
                    style={{...styles.openButton, backgroundColor: '#2196F3'}}
                    onPress={() => {
                      this.finishRoute();
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        fontSize: 14,
                      }}>
                      {this.state.started
                        ? 'Finish Current Package Delivery'
                        : 'Start the route'}
                    </Text>
                  </TouchableHighlight>
                </View>
              )}
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  backButtonImage: {
    marginLeft: 10,
    width: 25,
    height: 25,
    alignSelf: 'flex-end',
  },
  line: {
    backgroundColor: '#e5e8ec',
    width: 50,
    height: 5,
    borderRadius: 60,
    marginTop: 7,
    marginBottom: 25,
  },
  textView: {
    display: 'flex',
    flexDirection: 'column',
    margin: 10,
  },
  buttonsView: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 10,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: Math.round(Dimensions.get('window').height),
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
  },
  modalView: {
    // maxHeight: 300,
    width: '100%',
    backgroundColor: 'white',
    borderTopLeftRadius: 38,
    borderTopRightRadius: 38,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    alignSelf: 'center',
    backgroundColor: '#F194FF',
    borderRadius: 50,
    width: 265,
    height: 45,
    elevation: 2,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  modalText: {
    fontSize: 18,
    marginBottom: 15,
    textAlign: 'center',
    marginTop: 5,
  },
  headerText: {
    fontSize: 20,
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'notoserif',
    fontWeight: 'bold',
  },
  reviewText: {
    textAlign: 'center',
    fontSize: 13,
    color: '#569bf4',
  },
  header: {
    position: 'absolute',
    top: 5,
    margin: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    alignSelf: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  button: {
    marginRight: 5,
    marginLeft: 5,
    justifyContent: 'center',
    marginTop: 15,
    width: 100,
  },
  modalStarStyle: {
    height: 35,
    width: 35,
  },
  starContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'center',
  },
  stop: {
    margin: 5,
    borderBottomWidth: 1,
    borderColor: '#e5e8ec',
    display: 'flex',
    flexDirection: 'row',
    color: '#bcbcbc',
    width: Math.round(Dimensions.get('window').width),
  },
  // animatedView: {
  //   position: 'absolute',
  //   bottom: 0,
  //   width: '100%',
  //   transform: [
  //     {
  //       translateY: this.state.translateValue,
  //     },
  //   ],
  // },
});
