import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {Dimensions} from 'react-native';
import BackendApi from '../../services/api';
import moment from 'moment';
const width = Dimensions.get('window').width;

export default class History extends Component {
  constructor() {
    super();
    this.state = {
      location: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.07,
        longitudeDelta: 0.07,
      },
      history: [],
    };
  }
  componentWillMount(): void {
    BackendApi.getHistory().then(data => {
      console.log(data, 'reponseHistory');
      this.setState({
        history: data.packageHistories,
      });
    });
  }

  render() {
    if (this.state.history[0]) {
      return (
        <ScrollView style={styles.background}>
          <View style={styles.titleRow}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image
                style={styles.backButtonImage}
                source={{
                  uri:
                    'https://cdn.pixabay.com/photo/2016/09/05/10/50/app-1646213_960_720.png',
                }}
              />
            </TouchableOpacity>
            <Text style={styles.header}> History </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              margin: 10,
            }}>
            {this.state.history.map(item => {
              return (
                <View style={styles.container}>
                  <MapView
                    zoomEnabled={false}
                    toolbarEnabled={false}
                    rotateEnabled={false}
                    scrollEnabled={false}
                    pitchEnabled={false}
                    loadingEnabled={true}
                    showsUserLocation={false}
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    region={{
                      latitude: item.package.initialLatitude,
                      longitude: item.package.initialLongitude,
                      latitudeDelta: 0.07,
                      longitudeDelta: 0.07,
                    }}>
                    <Marker
                      coordinate={{
                        latitude: item.package.initialLatitude,
                        longitude: item.package.initialLongitude,
                      }}
                      pinColor={'#0366d6'}
                    />
                    <Marker
                      coordinate={{
                        latitude: item.package.destinationLatitude,
                        longitude: item.package.destinationLongitude,
                      }}
                      pinColor={'#f64f4f'}
                    />
                  </MapView>
                  <View style={styles.bottomView}>
                    <Text style={styles.text}>
                      {moment(item.createdAt).format('DD-MM-YYYY HH:mm')}
                    </Text>
                    <View style={styles.row}>
                      <Image
                        style={{
                          width: 20,
                          height: 20,
                          marginTop: 10,
                          alignSelf: 'center',
                        }}
                        source={require('../../../assets/blueIcon.png')}
                      />
                      <Text style={{alignSelf: 'center', fontSize: 15}}>
                        {item.package.initialLatitude +
                          ' ' +
                          item.package.initialLongitude}
                      </Text>
                    </View>
                    <View style={styles.row}>
                      <Image
                        style={{
                          width: 20,
                          height: 20,
                          marginTop: 10,
                          alignSelf: 'center',
                        }}
                        source={require('../../../assets/redIcon.png')}
                      />
                      <Text style={{alignSelf: 'center', fontSize: 15}}>
                        {item.package.destinationLatitude +
                          ' ' +
                          item.package.destinationLongitude}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      );
    } else {
      return <View />;
    }
  }
}
const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    marginTop: 15,
    borderRadius: 30,
    borderWidth: 1,
    height: 320,
    width: width - 30,
    overflow: 'hidden',
    borderColor: '#E6E6E6',
    elevation: 5,
    position: 'relative',
  },
  background: {
    backgroundColor: 'white',
    height: '100%',
    width: '100%',
  },
  bottomView: {
    display: 'flex',
    flexDirection: 'column',
    padding: 15,
    position: 'absolute',
    backgroundColor: 'white',
    height: 130,
    bottom: 0,
    width: width - 30,
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  row: {
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'row',
  },
  titleRow: {
    margin: 20,
    display: 'flex',
    flexDirection: 'row',
  },
  backButtonImage: {
    width: 29,
    height: 29,
    borderRadius: 75,
    borderWidth: 3,
    marginRight: 5,
    alignSelf: 'center',
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
  },
});
