import mixin from 'mixin-decorator';
import Axios from 'axios';
import {chatApi} from './chatApi';
import notificationsApi from './notificationsApi';
import mapsApi from './mapsApi';
import {usersApi} from './usersApi';
import {Alert} from 'react-native';
import {reviewsApi} from './reviewsApi';
import {packageApi} from './packageApi';

const REST_URI = 'https://truck-load.reigncode.dev/';

@mixin(usersApi, chatApi, reviewsApi, packageApi)
class Rest {
  AUTH_HEADER = 'Authorization';
  baseUrl: string;
  axios: Axios;

  constructor(baseURL: string) {
    this.baseUrl = baseURL;
    this.axios = Axios.create({baseURL, timeout: 20000});

    // AuthService.getToken().then(token => this.setToken(token));

    this.axios.interceptors.response.use(
      res => {
        return res;
      },
      error => this.catchError(error),
    );
  }

  setToken(token: string) {
    this.axios.defaults.headers.common[this.AUTH_HEADER] = token;
  }

  removeToken() {
    this.axios.defaults.headers.common[this.AUTH_HEADER] = null;
  }

  resHandler(res) {}

  catchError(err) {
    if (err) {
      return new Promise((resolve, reject) => {
        Alert.alert('Error', err.response.data.message || err, [
          {
            text: 'OK',
            onPress: () => reject(),
          },
        ]);
      });
    }
  }
}

export const BackendApi = new Rest(REST_URI);
export default BackendApi;
