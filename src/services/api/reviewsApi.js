export const reviewsApi = {
  addReview(toUserId, description, stars, packageId) {
    console.log(toUserId, description, stars, packageId, 'params');
    return this.axios
      .post(`reviews/${packageId}`, {toUserId: toUserId, description, stars})
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        return res.data;
      });
  },
  getReviewsByUserId(userId) {
    return this.axios.get(`reviews/${userId}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      return res.data;
    });
  },
  editReview(uuid, stars, description) {
    return this.axios
      .put(`reviews/${uuid}`, {stars, description})
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        return res.data;
      });
  },
  deleteReview(uuid) {
    return this.axios.delete(`reviews/${uuid}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      return res.data;
    });
  },
};
