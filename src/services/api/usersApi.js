//@flow
import {Linking} from 'react-native';
import {AuthService} from '../authService';
import {StateService} from '../state/stateService';
import messaging from '@react-native-firebase/messaging';
export const usersApi = {
  createNewUser(user) {
    return this.axios.post('auth/register', user).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
    });
  },
  async logout(token) {
    return this.axios
      .patch('auth/logout', {fcm_token: token})
      .then((res, err) => {
        if (err) {
          console.log(err.response.data.message);
        }
        console.log(res);
      });
  },

  async loginUser(credentials) {
    console.log(credentials, 'credentials');
    return this.axios.post('auth/login', credentials).then((res, err) => {
      if (err) {
        console.log(err, 'err')
        return this.catchError(err);
      }
      StateService.setField('userId', res.data.user.id);
      StateService.setField('token', res.data.token);
      this.resHandler(res.data.token);
      this.setToken(res.data.token);
      // StateService.setField('setTokenFunction', this.setToken);
      AuthService.setToken(res.data.token);
      AuthService.setUserId(res.data.user.id).then(r => {});
      return res.data;
    });
  },
  getUser(userId) {
    return this.axios.get(`users/${userId}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      return res;
    });
  },
  loginFacebook() {
    Linking.openURL('https://truck-load.reigncode.dev/auth/facebook').then(
      data => {},
    );
  },
  loginGoogle() {
    Linking.openURL('https://truck-load.reigncode.dev/auth/google').then(
      data => {},
    );
    // return this.axios.get(`auth/facebook`).then((res, err) => {
    //   if (err) {
    //     return this.catchError(err);
    //   }
    //   this.resHandler(res);
    //   return res;
    // });
  },
};
