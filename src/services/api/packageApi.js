import SocketApi from '../socket';
import BackendApi from './index';
import {StateService} from '../state/stateService';

export const packageApi = {
  addPackage(coords) {
    return this.axios.post('packages', coords).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.getAllPackages().then(data => {
        StateService.setField('packages', data);
      });
      this.resHandler(res);
      return res;
    });
  },
  editPackage(coords, packageId) {
    return this.axios.put(`packages/${packageId}`, coords).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      return res;
    });
  },
  getAllPackages() {
    return this.axios.get('packages').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      return res.data.packages;
    });
  },
  getPackageByOwnerId(ownerId) {
    return this.axios.get(`packages/owner/${ownerId}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      return res.data;
    });
  },
  requestPackage(packageId) {
    return this.axios.post(`package-requests/${packageId}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      BackendApi.getPackageRequestsOwner().then(data => {
        StateService.setField('ownerRequests', data.packageRequests);
      });
      BackendApi.getPackageRequestsCourier().then(data => {
        StateService.setField('courierRequests', data.packageRequests);
      });
      this.resHandler(res);
      return res.data;
    });
  },
  getPackageById(packageId) {
    return this.axios.get(`/packages/${packageId}`).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      return res.data.existingPackage;
    });
  },
  getPackageRequestsOwner() {
    return this.axios.get('/package-requests/owner').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      this.resHandler(res);
      StateService.setField('ownerRequests', res.data.packageRequests);
      return res.data;
    });
  },
  getPackageRequestsCourier() {
    return this.axios.get('/package-requests/courier').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      StateService.setField('courierRequests', res.data.packageRequests);
      this.resHandler(res);
      console.log(res.data.packageRequests, 'requestsssss');
      return res.data;
    });
  },
  deletePackageRequest(packageRequestId, body) {
    return this.axios
      .delete(
        `package-requests/${packageRequestId}${
          body && body.courierId ? '?courierId=' + body.courierId : ''
        }`,
      )
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        this.resHandler(res);
        BackendApi.getPackageRequestsOwner().then(data => {
          StateService.setField('ownerRequests', data.packageRequests);
        });
        BackendApi.getPackageRequestsCourier().then(data => {
          StateService.setField('courierRequests', data.packageRequests);
        });
        return res.data;
      });
  },
  setPackageCourier(packageId, courierId) {
    return this.axios
      .patch(`packages/${packageId}`, {courierId})
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        BackendApi.getPackageRequestsOwner().then(data => {
          StateService.setField('ownerRequests', data.packageRequests);
        });
        BackendApi.getPackageRequestsCourier().then(data => {
          StateService.setField('courierRequests', data.packageRequests);
        });
        this.resHandler(res);
        return res.data;
      });
  },
  setPackageDelivered(packageId, delivered) {
    return this.axios
      .patch(`packages/${packageId}/delivered`, {delivered})
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        this.resHandler(res);
        return res.data;
      });
  },
  courierConfirm(packageId, accepted) {
    return this.axios
      .patch(`packages/${packageId}/confirm`, {accepted})
      .then((res, err) => {
        if (err) {
          return this.catchError(err);
        }
        this.resHandler(res);
        return res.data;
      });
  },
  setStartedRoute(packageIds) {
    return this.axios.post('/package-routes', {packageIds}).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      console.log(res, 'res');
      this.resHandler(res);
      return res.data;
    });
  },
  addStopToStartedRoute(packageIds) {
    packageIds = packageIds.map(packageObject => packageObject.packageId);
    return this.axios.put('/package-routes', {packageIds}).then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      console.log(res, 'res');
      this.resHandler(res);
      return res.data;
    });
  },
  getStartedRoute() {
    return this.axios.get('/package-routes').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      console.log(res.data, 'sssssssss');
      this.resHandler(res);
      return res.data;
    });
  },
  getHistory() {
    return this.axios.get('/package-histories').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      console.log(res, 'sssssssss');
      this.resHandler(res);
      return res.data;
    });
  },
  deleteStartedRoute() {
    return this.axios.delete('/package-routes').then((res, err) => {
      if (err) {
        return this.catchError(err);
      }
      console.log(res, 'res');
      this.resHandler(res);
      return res.data;
    });
  },
  setFinishedRoute(packageId) {
    console.log(`/package-routes/${packageId}`, 'asdaskdfasjf');
    return this.axios.patch(`/package-routes/${packageId}`).then((res, err) => {
      console.log(res, 'res')
      if (err) {
        console.log('am intrat in eroare');
        return this.catchError(err);
      }
      this.resHandler(res);
      return res.data;
    });
  },
};
