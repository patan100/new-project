//@flow
import Baobab from 'baobab';

class Tree {
  token: string = null;
  userId: string = null;
  messageReceived: string = null;
  packages: Array = [];
  courierRequests: Array = [];
  ownerRequests: Array = [];
  startedRoute: boolean = false;
}
export const state = new Baobab(new Tree());
