import {state} from './State';
export class StateService {
  static setField(field, value) {
    state.select(field).set(value);
  }
}
