import AsyncStorage from '@react-native-community/async-storage';

const TOKEN = 'auth:token';
const USERID = 'auth:userId';

export class AuthService {
  static setToken(token) {
    try {
      return AsyncStorage.setItem(TOKEN, token);
    } catch (e) {
      console.log(e);
    }
  }

  static getToken() {
    try {
      return AsyncStorage.getItem(TOKEN);
    } catch (e) {
      console.log(e);
    }
  }

  static async logout() {
    return AsyncStorage.removeItem(TOKEN);
  }
  static async setUserId(userId) {
    try {
      return AsyncStorage.setItem(USERID, userId.toString());
    } catch (e) {
      console.log(e);
    }
  }
  static async getUserId() {
    try {
      return AsyncStorage.getItem(USERID);
    } catch (e) {
      console.log(e);
    }
  }
}
