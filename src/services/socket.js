import SocketIOClient from 'socket.io-client';
import {branch} from 'baobab-react/higher-order';
import {StateService} from './state/stateService';
import {AuthService} from './authService';
import BackendApi from './api';

class Socket {
  constructor() {
    this.socket = SocketIOClient('https://truck-load.reigncode.dev/');
    this.socket.on('message', res => {
      StateService.setField('messageReceived', res);
    });
    this.socket.on('create_package', res => {
      BackendApi.getAllPackages().then(data => {
        StateService.setField('packages', data);
      });
    });
    this.socket.on('package_request_list_updated', res => {
      BackendApi.getPackageRequestsOwner().then(data => {
        StateService.setField('ownerRequests', data.packageRequests);
      });
      BackendApi.getPackageRequestsCourier().then(data => {
        StateService.setField('courierRequests', data.packageRequests);
      });
    });
  }
  SocketSendMessage(chatId, text, fromUserId) {
    AuthService.getToken().then(token => {
      this.socket.emit('send_message', {chatId, text, fromUserId, token});
    });
  }
  SocketJoinChat(chatId) {
    this.socket.emit('join', chatId);
  }
  SocketEmitPackageRequestList() {
    AuthService.getUserId().then(userId => {
      this.socket.emit('package_request_list', {userId});
    });
  }
}

export const SocketApi = new Socket();
export default SocketApi;
