const { ENV_TYPE } = require('../android/app/src/config/conf');

const properties = {
    dev: require('./development.properties'),
    uat: require('./uat.properties'),
    production: require('./production.properties'),
};

module.exports = properties[ENV_TYPE || 'dev'];
